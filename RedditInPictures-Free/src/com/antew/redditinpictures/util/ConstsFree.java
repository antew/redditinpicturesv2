package com.antew.redditinpictures.util;

import com.antew.redditinpictures.library.utils.Consts;

public class ConstsFree extends Consts {
    public static final String ADMOB_ID            = "a14fd4522953241";
    public static final String DIALOG_UPGRADE      = "Upgrade";
    public static final String MARKET_INTENT       = "market://details?id=";
    public static final String REMOVE_ADS          = "removeAds";
    public static final String PRO_VERSION_PACKAGE = "com.antew.redditinpictures.pro";
}
